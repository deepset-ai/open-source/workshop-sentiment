# NLP workshop: Sentiment
Uni Münster Workshop on Sentiment Classification
## Getting started
If you are familiar with git, just clone the repository.  
You can also download the code as an archive with the "cloudy" button 
in the top right corner, next to the "Find File" button.

1. Install docker if it isn't on your system:  
Ubuntu: https://docs.docker.com/install/linux/docker-ce/ubuntu  
Windows: https://docs.docker.com/docker-for-windows/install/  
Mac: https://docs.docker.com/docker-for-mac/install/ 


2. Pull the latest version of the docker image by executing

    ```docker pull deepset/muensternlp:cpu```

3. Run the docker image with the downloaded code repository mounted into the container.
Change **/home/path/to/files** to the folder you downloaded "01 Workshop notebook" and "sb_Convnet.png" to.

    ```docker run -it -v /home/path/to/files/:/home/docker/code -p 8888:8888 -p 6006:6006 --name sentiment deepset/muensternlp:cpu```


You will see an output like:
``` 
[I 10:26:42.355 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
[I 10:26:42.515 NotebookApp] Serving notebooks from local directory: /home/docker/code
[I 10:26:42.515 NotebookApp] The Jupyter Notebook is running at:
[I 10:26:42.515 NotebookApp] http://(d6456518f1dd or 127.0.0.1):8888/?token=4aea80ffe1f1f0fcb1c782e300b25f89581bb777b2885f40
[I 10:26:42.516 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 10:26:42.516 NotebookApp] 
    
    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://(d6456518f1dd or 127.0.0.1):8888/?token=4aea80ffe1f1f0fcb1c782e300b25f89581bb777b2885f40
```

4. Connect to jupyter notebook in your browser with the above token - just use your local IP 127.0.0.1 and remove the rest inside the brackets:  
       ```http://127.0.0.1:8888/?token=4aea80ffe1f1f0fcb1c782e300b25f89581bb777b2885f40 ```


## Coding
Follow the instructions inside "01 Workshop notebook". **Enjoy coding!**  
Feel free to ask and interact. Python notebooks can show unexpected behaviour
and be quite confusing to beginners.

